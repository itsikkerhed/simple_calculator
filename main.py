import log
import simple_calculator

def runCalculator():
    print('\nWelcome to the simple calculator (write q to quit)')
    log.logging.info("Calculator started!")

    while True:
        try:
            inputs = [None, None, None]
            for i in range(3): #Get the 3 inputs
                print(simple_calculator.requestMessageDic[i])
                inputs[i] = input('> ')
                if inputs[i] == 'q':
                    print('goodbye...')
                    log.logging.info("Calculator exited!")
                    return 0
                else:
                    inputs[i] = float(inputs[i])

            result = simple_calculator.calculate(inputs)
            print(str(inputs[0]) + simple_calculator.operatorsDic[inputs[2]][1] + str(inputs[1]) + ' = ' + str(result))
            log.logging.info(str(inputs[0]) + simple_calculator.operatorsDic[inputs[2]][1] + str(inputs[1]) + ' = ' + str(result))
            print('restarting....')

        except ValueError as e:
            print('only numbers and "q" is accepted as input, please try again')
            log.logging.error(str(e.__class__) + ", " + str(e))

        except ZeroDivisionError as e:
            print('cannot divide by zero, please try again')
            log.logging.error(str(e.__class__) + ", " + str(e))
        
        except KeyError as e:
            print('invalid operator request, please try again')
            log.logging.error(str(e.__class__) + ", " + str(e))

def main():
    runCalculator()

if __name__ == "__main__":
    main()