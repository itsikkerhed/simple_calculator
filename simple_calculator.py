import operator

requestMessageDic = {
    0: "enter the first number",
    1: "enter the second number",
    2: "would like to: 1. add 2. subtract 3. divide or 4. multiply the numbers?"
}
operatorsDic = {
    1: [operator.add, ' added with '],
    2: [operator.sub, ' subtracted from '],
    3: [operator.truediv, ' divided with '],
    4: [operator.mul, ' multiplied with ']
}

def calculate(inputs):
    return operatorsDic[inputs[2]][0](inputs[0], inputs[1]) # Example, when using operator 1, it's basically: operator.add(inputs[0], inputs[1])